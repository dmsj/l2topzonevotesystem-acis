package net.sf.l2j.gameserver.vote.handler;

/**
 * @author L2Cygnus
 */
public interface IVoteHandler {
	boolean isVoted(String ip);
	int getTotalVotes();
}
